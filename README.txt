USAGE

    kutoenclave new|read|edit|close <ENCLAVE-NAME>

This tool sets up and manages small luks-based encrypted filesystems.
This tool is specifically designed allow tools that assume read/write
access to filesystems (e.g. gpg) to transparently work with with
read-only enclaves; Written data is retained while the enclave is 
mounted then discarded once the enclave is closed.

DEPENDENCIES
    
    cryptsetup(1)
    losetup(1)
    mkfs.ext4(1)
    coreutls
    Overlay Support

COMMANDS
    
    new     Generates a new enclave named <ENCLAVE-NAME>. By default 
            the enclave is ~10MiB and is stored in a file of the 
            same name.
            
    edit    Mount the enclave in read-write mode.
    
    read    Mount the enclave in read-only mode.
    
    close   Unmount and lock the enclave.
    
    help    Show this help message
    
    version Show tool version

ENVIRONMENT VARIABLES
    
    KA_SECRETS_MOUNT_DIR                Where enclaves are mounted. 
                                        Defaults to /mnt/secrets.
                                        
                                        Do not modify this variable
                                        while any enclave is mounted.
    
    KA_SECRETS_DEFAULT_ENCLAVE_SIZE     The default size of enclaves 
                                        generated with the new 
                                        command in increments of 
                                        1kiB.  
                                        
                                        Overall size will be 
                                        
                                              `N * 1024 + 1024`  
                                        
                                        Actual size will be smaller. 
                                        Defaults to 10000 (~10MiB).

COPYRIGHT

(ح) حقوق المؤلف محفوظة لشركة كوتوميتا لبرمجة وتشغيل الكمبيوتر وتصميم 
وإدارة مواقع الإنترنت (ش.ش.و) - سنة ٢٠٢٠

تنفي الشركة أي مسئولية عن عدم أهلية البرنامج لأداء وظيفته المعلن عنها 
أو عن الأضرار التي قد يتكبدها المستخدم وغيره نتيجة استخدام هذا 
البرنامج. تقع مسؤولية أي ضرر ناجم عن استخدام هذا البرنامج على عاتق 
المستخدم وحده. اطلع على المستندات المرافقة لهذا البرنامج لمزيد من 
المعلومات.

Copyright © 2020 Kutometa SPLC, Kuwait
All rights reserved. Unless explicitly stated otherwise, this program 
is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE 
WARRANTY OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
See accompanying documentation for more details.


Kutometa SPLC
contact@ka.com.kw
www.ka.com.kw
